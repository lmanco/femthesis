# README #

## Português brasileiro ##

Classe em LaTeX para confecção de teses de mestrado ou doutorado, de acordo com as normas da Faculdade de Engenharia Mecânica da Universidade Estadual de Campinas.

A classe provida (femthesis.cls) possui todos os comandos para geração das seções frontais e traseiras automaticamente, como capa, contra-capa, folha de assinatura etc. No momento a classe tem duas opcões adicionais para modificara geração automática: englishthesis e doutorado (phd); cuja função é mudar a geração automática de conteúdo. Um exemplo de documento principal está 
disponível como example.tex, até que a documentação seja feita.

Caso queira ajudar on projeto. Lembre-se que o objetivo final é promover essa classe como uma planta-baixa para futuros trabalhos. Nesse contexto, quanto menos pacotes, melhor.

### Aviso Legal ###

Este repositório é uma cópia de outro repositório privado de qual o autor atual faz parte. Como o administrador do repositório anterior planejava deixar o projeto público, porém não concretizou seu objetivo, esta é a publicação da classe terminada pelo autor atual.

## English ##

LaTeX class for typesetting master's and doctor's thesis, according to the rules of Faculty of Mechanical Engineering of Univesity of Campinas.

The provided class (femthesis.cls) contains all commands for automatic generation of front and back matter such as titlepage, signatures page etc. At the moment, the class has two additional options to modify its generation: englishthesis and phd (doutorado); which change how automatic generation from meta data is to be done. An example document is available as exmaple.tex, until any actual documentation is done.

In case you want to help in this project. Remember that the final goal is to promote this class as a foundation for future works. In this context, the lesser the packages, the better.

### Disclaimer ###

This repository is a copy of another private repository which the current autor is part of. As the admin of the first repository planned to make it public but didn't pursue the project further, this is the publication of the finished class by the current author.

# Pacotes necessários / Necessary Packages #

* graphicx
* fontenc (luatex) or/ou inpuntec (pdflatex)
* geometry
* fancyhdr
* titlesec

### Version 1.0-rc2 ###

### Last changes ###
* 11/05/2017: Babel and datetime are no more needed, as a simple function for date creation has been made.

#### Authors ####
* Marcos Mendel: Author of the original Thesis template FEM released.
* Rodolfo Jordão: Author of the many changes from the original Thesis template FEM released. Current main author.
* "Wendell" (thejesusbrr): Owner of the original bitbucket repo this repo is itself a fork-copy of.
